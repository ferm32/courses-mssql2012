MS SQL Server 1.0 1989
Express Editions 2008 4Gb
Express Editions 2012,2014 10Gb
RAM Enterprise Editions - определяется возможностями ОС
OLTP online transacton processing
Транзакция - совокупность команд, выполняемых как единое целое.
View - выбирает данные
Хранимая процедура - может делать INSERT, DELETE, UPDATE
Функция - не может изменять данные, иногда её называют
"View-шкой с параметрами"

Databases in SQL Server are:
Containers: Tables, Views, Procedures, Functions, Users, Roles, Schemas
Boundaries: Security Accounts, Permissions, Default collation
Backed by Files: ...

Collation - языковые параметры
БД - пользовалельские, системные.
Constrain-ы - ограничители целостности, например Primary key (первичный
ключ) - гарантирует уникальность записей, Foreign key (вторичный ключ) -
ссылка на запись другой таблицы, как правило на первичный ключ.

Категории операторов T-SQL
==========================
DML Data Manipulation Language - в основном будем использовать его:
SELECT, INSERT, UPDATE, DELETE
DDL Data Definition Language:
CREATE,  ALTER, DROP
DCL Data Control Language:
GRANT, REVOKE, DENY

Элементы T-SQL
==============
Предикаты и операторы
Функции
Переменные
Выражения
Разделители выполнения (Batch Separators)
Контроль потоков (если то иначе)
Комментарии

Предикаты и операторы
---------------------
IN, BETWEEN, LIKE
IN - среди того-то того-то
BETWEEN - диапазон
LIKE - поиск и сравненение с символьными данными
= > < <= >= <>
AND OR NOT
+ - * / %
Присоединение +

Функции
-------
SUBSTRING, LEFT, RIGHT, LEN, DATALENGTH, REPLACE, UPPER, LOWER, RTRIM, LTRIM
GETDATE, SYSTDATETIME, GETUTCDATE, DATEADD, DATEDIFF, YEAR, MONTH, DAY
SUM, MIN, MAX, AVG, COUNT

Функции - скалярные, табличные
Функции - детерменированные, недетерминированные - возвращают
всё время одно и то же или разное (например, выдают дату)

Контроль потока данных, ошибки, транзакции
------------------------------------------
IF...ELSE, WHILE, BREAK, CONTINUE, BEGIN...END
TRY...CATCH
BEGIN TRANSACTION, COMMIT TRANSACTION, ROLLBACK TRANSACTION

GO это не команда, это разделение блоков выполнения команд (пакета)
(областей видимости)
В командной строке команды не выполняеются до введения GO

Комментарии
-----------
--
/* можно многострочные */

Наборы данных
-------------
Курсоры - рекомендуется использовать как можно меньше.

Логические предикаты
--------------------
Фильтрация WHERE HAVING
Условная логика CASE
Объединение таблиц (фильтр ON)
Определение подзапросов
Проверка целостности данных CHECK
Контроль потока IF

Элементы оператора SELECT
-------------------------
SELECT <select list?
FROM <table source>
WHERE <search condition>
GROUP BY <group by list>
HAVING <search condition>

SELECT
======
SELECT companyname, country
    FROM Sales.Customers;

SELECT *
    FROM [mia-sql1\mktg1].tsql2012.Sales.Orders;
    -- имя машины, инстанса, БД, схемы и таблицы
    -- Если параметр не указан, он принимается по дефолту

SELECT DISTINCT country
-- вывести только уникальные строки
    FROM sales.Customers
    GROUP BY counry;
-- группировать по странам

Алиасы
------
SELECT empid as employeeid, firstname as given, lastname as surname
    FROM HR.Employees;

SELECT HE.empid as employeeid, HE.firstname as given, HE.lastname as surname
    FROM HR.Employees as HE;
    -- алиас для таблицы
    -- FROM выполняется первым, а для использования алиасов важен порядок.
Варианты:
- HR.Employees HE
- HR.Employees as HE
- HE = HR.Employees

NB Для выделения прямоугольной области кода мышкой держите нажатой Alt
NB При формировании запроса SELECT можно сформировать список полей, перетащив
папочку Columns из Object Explorer в окно запроса.

NB Для подключения подсказок IntelliSense, нажмите Ctrl+Q+I.
После этого для получения меню продолжения ввода нажмите Ctrl+J

NB алиасы, содержащие служебные слова или пробелы, заключаются в
квадратные скобки:
... c.companyname AS [Company Name] ...

JOIN-ы
======
Cross join - объединение всего со всем ("декартово произведение"),
очень много строк результата
Inner join - точное условие совпадения (к.-л. строки есть и там, и там)
outer join - неточное условие совпадения, бывает Left, Right или Full

При многотабличном объединении нет гарантии, что оно пройдёт в
объявленном порядке. Внутренний оптимизатор сервера может изменить
заданный в SELECT порядок присоединения таблиц.

ORDER BY, WHERE - сортировка, фильтрация
========================================
ORDER BY отрабатывает последним (даже после SELECT! -> в нём допустимы
любые алиасы, даже определённые в самом SELECT).


...
ORDER BY hiredate DESC, lastname ASC;

NB Display Estimated Execution Plan - отобразить ожидаемый план
выполнения, с нагрузкой - в контекстном меню запроса

...
WHERE country = N'Spain';

или
...
WHERE country IS N'Spain';

N - тип данных Unicode, т.е. включая национальные символы.

...
WHERE country IN (N'UK', N'Spain');

WHERE country NOT IN (N'UK',N'Spain');
-- Пустые значения страны не выведутся!!!

NB Если не даёт изменить структуру таблицу, поискать в опциях
запрет на изменение структуры, требующего пересоздания таблицы

BETWEEN ... AND ... - аналог <= ... <= (интервал включая границы)

SELECT TOP (n) field1, field2, ...
При выборе через TOP имеет значение порядок отображения записей в таблице
Т. е. таблица сначала сортируется, а потом выводятся первые n значений

SELECT TOP (n) WITH TIES field1, ...
Добавить в конец те строчки, которые совпадают с последней строчкой
для поля сортировки

SELECT ...
...
ORDER BY ...
OFFSET 50 ROWS FETCH NEXT 50 ROWS ONLY;
-- отрабатывается после сортировки.

NB - вбить кувалдой в голову NULL - не нулевые, а пустые значения.

...
WHERE region IS NOT NULL;

В условиях WHERE % обозначает строку любого размера, включая пустую,
а _ обозначает любой символ.

Collationаs (языковые параметры)
===============================
Типы данных:
nchar - national char (UNICODE)
nvarchar - national varchar (UNICODE)
nchar(5) - 5 символов

Типы данных
===========
Числовые:
tinyint, smallint, int, Bigint, bit, decimal/numeric, money, smallmoney

binary(n)
varbinary(n)
varbinary(MAX)

Другие:
rowversion, uniqueidentifier, xml, cursor, hierarchyid, sql_variant, table

uniqueidentifier использовать в качестве PRIMARY KEY нехорошо.

xml - очень спорный тип данных. Много думать, прежде чем использовать.
select *
    from Production.Products
    for xml auto, elements

Символьные
----------
CS (Case sensitivity) - чувствительный к регистру
AS (Accent sensitivity) - чувствительность к дискретным символам.
В Русском языке ё, й
CI, AI (case, accent insensitivity) - не чувствительные

Дата-время
----------
Date, Datetime2 - до MS SQL 2008
В 2008 появились Date Time
Сейчас самые распростанённые Smalldatetime (точноть 1 минута)
и Datetimeoffset (100 наносекунд + часовой пояс) - удобно для банков

DATEADD(), EOMONTH(), DATEDIFF(), ISDATE(),
DATEFROMPARTS(), DATE2FROMPARTS()

NB! CAST( expresstion AS data_type [(length)]
- преобразование типа данных

NB! Алиасы, объявленные в пределах одной из фаз выполнения логического
запроса, не могут использоваться в этой же фазе, так как условно говоря
выполняются одновременно. Поэтому, к примеру, алиас колонки, объявленный
на фазе SELECT, не может быть использован в этой же фазе.

Помимо функций CAST() и CONVERT(), в MS SQL 2012 появилась новая функция
TRY_CONVERT(type, data). В случае неуспеха возвращает NULL.

New in MS SQL 2012 EOMONTH()

sql-server-helper.com

Функция FORMAT() (new in 2012) позволяет конвертировать дату
в кириллическое представление (ru_RU). Например, 13 августа 2014 года.
CONVERT позволяет задать формать вывода даты - американский,
французский, английский и т. п.
